<?php
/* Template Name: Contact
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mindenergy
 */

get_header(); ?>

<!-- Contact Us Heading -->
<div class="section floral-green heading text-center">
  <div class="container heading-container">
    <div class="row">
      <div class="col-md-12">
          <h2>- CONTACT US -</h2>
      </div>
    </div>
  </div>
</div>

<div class="content-section">
	<div class="container content-area">
		<div class="col-md-4">
			<?php
				while ( have_posts() ) : the_post(); 
				the_content(); 
				endwhile; ?>
		</div>		
		<div class="col-md-6">
			<?php the_field('contact_form'); ?>
		</div>
			
	</div><!-- #primary -->
</div>

<?php
add_filter( 'template_include', 'include_template_function', 1 );
get_footer();
