<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mindenergy
 */

get_header(); ?>

	<!-- hero -->
<div class="cover">
  <video autoplay="" loop="" id="vid" class="hidden-xs">
    <source src="<?php bloginfo('template_directory'); ?>/video/wedding-comp.mp4" type="video/mp4">
  </video>
  <div class="container cover-caption">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1><?php bloginfo('name'); ?></h1>
        <p><?php bloginfo('description'); ?></p>
        <br>
        <br>
        <a class="btn btn-lg btn-primary" href="contact">GET STARTED</a>
      </div>
    </div>
  </div>
</div>

<!-- Services Sub Nav -->
<?php get_template_part('template-parts/services-subnav' ); ?>

<!-- testimonials -->
<div class="section testimonials-container padded">
  <div class="container">
    <div class="row">
      <?php $args = array('post_type' => 'Testimonial', 'posts_per_page' => 2); $posts = get_posts( $args); foreach($posts as $post): setup_postdata( $post ); ?>
      <div class="col-md-6">
        <div class="well">
          <?php the_content(); ?>
          <h3><?php the_title(); ?></h3>
        </div>
      </div>
      <?php endforeach; wp_reset_postdata(); ?>
    </div>
  </div>
</div>

<!-- cta -->
<div class="section cta-container padded">
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<div class="cta-bg" style="background-image: url('<?php echo $thumb['0'];?>')"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <?php the_content(); ?>
          <br>
        <a class="btn btn-lg btn-primary" href="contact">LET'S TALK</a>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>