if (screen.width > 768) {

  jQuery(window).scroll(function() {
  if (jQuery(document).scrollTop() > 50) {
    jQuery('nav').addClass('shrink');
    jQuery('.cover').addClass('shrink-cover');
    jQuery('.heading').addClass('shrink-heading');
  } else {
    jQuery('nav').removeClass('shrink');
    jQuery('.cover').removeClass('shrink-cover');
    jQuery('.heading').removeClass('shrink-heading');
  }
  });
}