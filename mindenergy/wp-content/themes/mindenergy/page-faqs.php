<?php
/* Template Name: FAQs
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mindenergy
 */

get_header(); ?>

<!-- Contact Us Heading -->
<div class="section floral-green heading text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <h2>- FAQS -</h2>
      </div>
    </div>
  </div>
</div>

<div class="content-section">
	<div class="container content-area">
		<div class="col-md-4">
			<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
		</div>		

		<div class="col-md-8">
			<div class="well">
		<?php $args = array('post_type' => 'FAQ', 'posts_per_page' => 999); 
		$posts = get_posts( $args); foreach($posts as $post): 
		setup_postdata( $post ); ?>
			<h3><?php the_field('faq_question'); ?></h3>
			<p class="lead"><?php the_field('faq_answer'); ?></p>
		<?php endforeach; wp_reset_postdata(); ?>
			</div>
		</div>
		
			
	</div><!-- #primary -->
</div>

<?php
add_filter( 'template_include', 'include_template_function', 1 );
get_footer();
