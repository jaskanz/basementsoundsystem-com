<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mindenergy
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="<?php bloginfo('url'); ?>">MIND ENERGY PRODUCTIONS</a>
    </div>
    <div class="collapse navbar-collapse" id="navbar-ex-collapse">
      <div class="row hidden-xs text-center">
        <a href="<?php bloginfo('url'); ?>">
          <img src="<?php the_field('logo', 'option'); ?>" class="center-block img-responsive nav-logo">
          </a>
      </div>
      <div class="row navbar-list">
		<?php
		wp_nav_menu( array(
		'theme_location' => 'primary',
		'depth' => 2,
		'container' => false,
		'menu_class' => 'lead nav navbar-nav',
		'fallback_cb' => 'wp_page_menu',
		//Process nav menu using our custom nav walker
		'walker' => new wp_bootstrap_navwalker())
		);
		?>
      </div>
    </div>
  </div>
</nav>
