<?php
/* Template Name: Service
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mindenergy
 */

get_header(); ?>

<!-- Contact Us Heading -->
<div class="section floral-blue heading text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <h2>- <?php the_title(); ?> -</h2>
      </div>
    </div>
  </div>
</div>

<div class="content-section">
	<div class="container content-area">
		<div class="row">
			<div class="col-md-4">
				<?php the_post_thumbnail('full', array('class' => 'center-block img-responsive service-icon-interior')); ?>
			</div>		
			<div class="col-md-6">
				<?php
					while ( have_posts() ) : the_post(); 
					the_content(); 
					endwhile; ?>
			</div>
		</div>
	</div><!-- #primary -->
</div>

<?php
add_filter( 'template_include', 'include_template_function', 1 );
get_footer();