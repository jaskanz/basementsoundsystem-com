<!-- services -->
<div class="section floral-blue home-heading text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <h2>- OUR SERVICES -</h2>
      </div>
    </div>
  </div>
</div>

<div class="section services-container">
  <div class="container">
	<div class="row">
		<?php $args = array('post_type' => 'Service', 'posts_per_page' => 3); $posts = get_posts( $args)
		; foreach($posts as $post): setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>" >
		<div class="col-md-4 text-center">
			<div class="service">
				<?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'svg img-responsive center-block service-icon')); ?>          		
				<h2><?php the_title(); ?></h2>
				<p><?php the_excerpt(); ?></p>
        	</div>
        </div>
		</a>
		<?php endforeach; wp_reset_postdata(); ?>
	</div>
  </div>
</div>