<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mindenergy
 */

?>

<!-- footer -->
<footer class="section footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 footer-left text-center">
        <img class="center-block" src="<?php bloginfo('template_directory'); ?>/img/mind-energy-logo-reverse.svg" alt="">
        <p>Contact information goes here
          <br>Line 2 of contact information
          <br>Line 3 of contact information</p>
      </div>
      <div class="col-sm-4 footer-middle hidden-xs">
        <?php
        wp_nav_menu( array(
        'theme_location' => 'secondary',
        'depth' => 2,
        'container' => false,
        'menu_class' => 'nav',
        'fallback_cb' => 'wp_page_menu',
        //Process nav menu using our custom nav walker
        'walker' => new wp_bootstrap_navwalker())
        );
        ?>
      </div>
      <div class="col-sm-4 footer-right">
        <div class="row">
          <div class="col-md-12 hidden-lg hidden-md hidden-sm text-center">
            <?php if(get_field('me_instagram', 'option')): ?>
            <a href="<?php the_field('me_instagram', 'option') ?>" target="_blank"><i class="fa fa-2x fa-fw fa-instagram"></i></a>
            <?php endif; ?>
            <?php if(get_field('me_twitter', 'option')): ?>
            <a href="<?php the_field('me_twitter', 'option') ?>" target="_blank"><i class="fa fa-2x fa-fw fa-twitter"></i></a>
            <?php endif; ?>
            <?php if(get_field('me_facebook', 'option')): ?>
            <a href="<?php the_field('me_facebook', 'option') ?>" target="_blank"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 hidden-xs text-right">
            <?php if(get_field('me_instagram', 'option')): ?>
            <a href="<?php the_field('me_instagram', 'option') ?>" target="_blank"><i class="fa fa-2x fa-fw fa-instagram"></i></a>
            <?php endif; ?>
            <?php if(get_field('me_twitter', 'option')): ?>
            <a href="<?php the_field('me_twitter', 'option') ?>" target="_blank"><i class="fa fa-2x fa-fw fa-twitter"></i></a>
            <?php endif; ?>
            <?php if(get_field('me_facebook', 'option')): ?>
            <a href="<?php the_field('me_facebook', 'option') ?>" target="_blank"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
