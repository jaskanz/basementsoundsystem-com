<?php
/**
 * Template Name: Members
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>

<div class="container-fluid interior-header">
    <div class="container">
        <h2>MEMBERS</h2>
    </div>
</div>
<?php get_template_part('template-parts/members-loop' ); ?>
<?php get_footer(); ?>