<?php
/**
 * Template Name: Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>

<?php 
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
?>

<!-- Hero -->
<div class="container-fluid parallax-window" id="hero" data-bleed="50" data-parallax="scroll" data-image-src="<?php echo $src[0]; ?>">
    <div class="row">
        <div class="container">
            <div class="col-sm-6">
                <h1><?php bloginfo('name'); ?></h1>
                <hr>
                
                <?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); 
						the_content();
					} 
				} ?>
                
                <a class="btn" href="<?php bloginfo( 'url'); ?>/contact"><i class="fa fa-envelope"></i>CONTACT US</a>
            </div>
        </div>
    </div>
</div>

<!-- how / about section -->
<div class="container-fluid center" id="how">
    <div class="container">       
        <h3><?php the_field('about_text'); ?></h3>
        <a class="btn" href="<?php bloginfo( 'url'); ?>/events"><i class="fa fa-calendar"></i>VIEW EVENTS</a>
    </div>
</div>

<!-- Members Section -->
<div class="container-fluid interior-header">
    <div class="container">
        <h2>MEMBERS</h2>
    </div>
</div>
<?php get_template_part('template-parts/members-loop' ); ?>

<!-- CTA -->
<?php get_template_part('template-parts/cta-loop' ); ?>

<?php get_footer(); ?>