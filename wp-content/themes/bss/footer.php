</div><!-- #main -->
 
<footer id="footer">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-6 hidden-xs">
                        <a class="footer-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                        <br>
                        <small>Copyright &copy2015 Basement Sound System</small>
                    </div>
                    
                    <div class="col-md-6 hidden-sm hidden-xs text-right">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-nav', 'menu_id' => 'main-menu', 'menu_class' => 'nav navbar-nav navbar-right', 'container' => 'nav' ) ); ?>
                    </div>
                    
                    <div class="col-md-6 text-right pull-right" id="social">
                        <a href="http://www.facebook.com/BasementSoundSystem" target="_blank"><i class="fa fa-2x fa-fw fa-facebook-square"></i></a>
                        <a href="http://www.twitter.com/bssdjcrew" target="_blank"><i class="fa fa-2x fa-fw fa-twitter-square"></i></a>
                        <a href="http://www.instagram.com/bssdjcrew" target="_blank"><i class="fa fa-2x fa-fw fa-instagram "></i></a>
                    </div>
                
                </div>
                <hr>
                <div class="col-sm-6 visible-xs center">
                        <small>Copyright &copy2015 Basement Sound System</small>
                    </div>
            </div>
        </footer>


</body>

<?php wp_footer(); ?>
</html>