<?php
/**
 * Template Name: About
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );?>

<div class="container-fluid interior-header">
	<div class="container">
	    <h2><?php the_title(); ?></h2>
    </div>
</div>


<div class="container-fluid about-container">  
	<div class="container">

		<div id="about-hero" class="col-sm-3" style="background-image: url(<?php echo $src[0]; ?> )">
		</div>        

        <div class="col-sm-6">
        <?php 
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); 
				the_content();
			} 
		} ?>
		</div>
		<div class="col-sm-3"></div>
        
	</div>
</div>

<?php get_footer(); ?>