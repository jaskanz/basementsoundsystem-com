<?php
/*
 *
 * Single Post Template: Event
 *
 * Description: This part is optional, but helpful for describing the Post Template
 *
*/

get_header(); ?>

<div class="container-fluid interior-header">
	<div class="container">
	    <h2><?php the_title(); ?></h2>
    </div>
</div>

<div class="container-fluid bg-shade padded">
<div class="container event-container">
		<div class="row">
			<div class="col-md-3">
				<a href="<?php the_field('event_flyer'); ?>" <?php if(get_field('event_flyer')): ?>
					rel="lightbox"<?php endif; ?>>
					<?php if(get_field('event_flyer')) { $url = get_field('event_flyer');} else { $url = get_template_directory_uri() . "/img/bss-default.jpg";} ?>
					<img class="img-responsive" src="<?php echo $url; ?>" />
				</a>
			</div>
			<div class="col-md-9 event-details">
				<h3><?php the_title(); ?></h3><?php echo synved_social_share_markup(); ?>	<hr>
				<p><strong><?php the_field('event_date'); ?></strong> at </p>
				<!-- Event Venue -->
				<?php $post_objects = get_field('event_venue'); if( $post_objects ): ?>
			    <?php foreach( $post_objects as $post_object): ?>
			    <a href="<?php the_field('venue_website', $post_object); ?>" target="_blank">
			    <?php echo get_the_title($post_object); ?>
				</a>
			    <?php endforeach; ?>
				<?php endif; ?>
				<br>
				<?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); 
						the_content();
					} 
				} ?>
				<div class="facebook-button">
					<?php if (get_field('event_facebook_page')): ?>
					<a href="<?php the_field('event_facebook_page'); ?>" target="_blank"><button class="btn btn-primary">FACEBOOK EVENT PAGE</button></a>
					<?php endif; ?>
				</div>

				<!-- Event Dj's -->
				<?php $post_objects = get_field('event_djs'); if( $post_objects ): ?>
			    <div class="row text-center event-members">
			    <?php foreach( $post_objects as $post_object): ?>
			    	<div class="col-md-2 col-xs-6">
			    	<a href="<?php echo get_permalink($post_object); ?>">
				    	<img src="<?php the_field('member_icon', $post_object); ?>">
				        <?php echo get_the_title($post_object); ?>
			        </a>
			        </div>
			    <?php endforeach; ?>
			    </div>
				<?php endif;?>

			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>