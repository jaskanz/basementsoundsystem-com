<?php
/*
 *
 * Single Post Template: Member
 *
 * Description: This part is optional, but helpful for describing the Post Template
 *
*/

get_header(); ?>

<?php if(get_field('cover_photo')): ?>
	<div class="container-fluid interior-header parallax=window cover-photo" data-bleed="50" data-parallax="scroll" data-image-src="<?php the_field('cover_photo'); ?>">
		<div class="container">
		    <h2></h2>
	    </div>
	</div>
<?php endif; ?>

<!-- container-fluid -->
<div class="container-fluid member-container">
	<!-- container -->
	<div class="container">
		<div class="row">
			<!-- picture column -->
			<div class="col-sm-3 member-col center">
				<!-- member picture -->
				<div class="member-pic">
					<?php the_post_thumbnail('full'); ?>
				</div>

				<!-- social icons -->
				<div class="member-social">
					<?php $fb = get_field('member_facebook'); $tw = get_field('member_twitter'); $in = get_field('member_instagram'); $em = get_field('member_email'); $social = array($fb, $tw, $in, $em);?>
					<?php if(!empty($social)): ?>
				 	<ul class="social-links">
				 		<?php if($fb) { echo '<li><a target="_blank" href="'.$fb.'"><i class="fa fa-facebook-square fa-2x"></i></a></li>'; } ?>
				 		<?php if($tw) { echo '<li><a target="_blank" href="'.$tw.'"><i class="fa fa-twitter-square fa-2x"></i></a></li>'; } ?>
				 		<?php if($in) { echo '<li><a target="_blank" href="'.$in.'"><i class="fa fa-instagram fa-2x"></i></a></li>'; } ?>
				 		<?php if($em) { echo '<li><a target="_blank" href="mailto:'.$em.'"><i class="fa fa-envelope fa-2x"></i></a></li>'; } ?>
				 	</ul>
					<?php endif; ?>
				</div>
			</div>

			<!-- about column -->
	        <div class="col-sm-9 member-about">
	        	<img class="member-icon" src="<?php the_field('member_icon') ;?>" alt="">
				<h4><?php the_title(); ?></h4>
				<hr>
		        <?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						the_content();
					}
				} ?>
			</div>
		</div>
	</div>
</div>

<?php get_template_part('template-parts/events-loop' ); ?>

<?php get_footer(); ?>
