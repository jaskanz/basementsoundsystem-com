<?php $postid = $post->ID; ?>
<?php $page = is_page(); ?>
<?php $single = is_single(); ?>

<div class="container-fluid bg-shade padded">  

<!-- repeat event -->
	<?php $repeat_args = array(	'post_type' 		=> 	'event',
								'meta_key'			=>	'event_repeat',
								'meta_value'		=>	TRUE,
								'status'			=>	'published',
								); ?>
	<?php $repeat_posts = get_posts($repeat_args); ?>
	<?php if ($repeat_posts): ?>
		<?php foreach ($repeat_posts as $post): setup_postdata( $post );?>
			<?php if ($page || in_array($postid, get_field('event_djs'))): ?>
			<!-- Event Container -->
				<div class="container event-container">
					<div class="row">
						<div class="col-md-3">
							<a href="<?php the_field('event_flyer'); ?>" <?php if(get_field('event_flyer')): ?>
								rel="lightbox"<?php endif; ?>>
								<?php if(get_field('event_flyer')) { $url = get_field('event_flyer');} else { $url = get_template_directory_uri() . "/img/bss-default.jpg";} ?>
								<img class="img-responsive" src="<?php echo $url; ?>" />
							</a>
						</div>
						<div class="col-md-9 event-details">
							<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a><?php echo synved_social_share_markup(); ?><hr>
							<p><strong>Every <?php the_field('event_repeat_day'); ?></strong> at </p>
							<!-- Event Venue -->
							<?php $post_objects = get_field('event_venue'); if( $post_objects ): ?>
						    <?php foreach( $post_objects as $post_object): ?>
						    <a href="<?php the_field('venue_website', $post_object); ?>" target="_blank">
						    <?php echo get_the_title($post_object); ?>
							</a>
						    <?php endforeach; ?>
							<?php endif; ?>
							<br>
							<?php the_content() ?>

							<div class="facebook-button">
								<?php if (get_field('event_facebook_page')): ?>
								<a href="<?php the_field('event_facebook_page'); ?>" target="_blank"><button class="btn btn-primary">FACEBOOK EVENT PAGE</button></a>
								<?php endif; ?>
							</div>

							<!-- Event Dj's -->
							<?php $post_objects = get_field('event_djs'); if( $post_objects ): ?>
						    <div class="row text-center event-members">
						    <?php foreach( $post_objects as $post_object): ?>
						    	<div class="col-md-2 col-xs-6">
						    	<a href="<?php echo get_permalink($post_object); ?>">
							    	<img src="<?php the_field('member_icon', $post_object); ?>">
							        <?php echo get_the_title($post_object); ?>
						        </a>
						        </div>
						    <?php endforeach; ?>
						    </div>
							<?php endif; ?>

						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php endforeach; wp_reset_postdata();?>
	<?php endif; ?>

<!-- events -->
	<?php $today = date('Ymd'); ?>
	<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; ?>
	<?php $args = array( 'post_type' 		=> 	'event',
						 'paged'			=> 	$paged,
						 'meta_key'			=> 	'event_date',
						 'meta_query' 		=> 	array(
											   		array(
											        	'key'		=> 'event_date',
											        	'compare'	=> '>=',
											        	'value'		=> $today,
											    	),
											    	array(
											    		'key'		=> 'event_repeat',
											    		'compare'	=> '!=',
											    		'value'		=> TRUE,
											    	),
												),
					     'orderby'			=> 	'meta_value_num',
					     'posts_per_page' 	=> 	999,
						 'order'			=> 	'ASC'); ?>
	<?php query_posts($args); ?>
	
		<?php if (have_posts()): ?>

			<?php while (have_posts() ) : the_post(); ?>
				<?php if ($page || in_array($postid, get_field('event_djs'))): ?>
				
			<!-- Event Container -->
				<div class="container event-container">
					<div class="row">
						<div class="col-md-3">
							<a href="<?php the_field('event_flyer'); ?>" <?php if(get_field('event_flyer')): ?>
								rel="lightbox"<?php endif; ?>>
								<?php if(get_field('event_flyer')) { $url = get_field('event_flyer');} else { $url = get_template_directory_uri() . "/img/bss-default.jpg";} ?>
								<img class="img-responsive" src="<?php echo $url; ?>" />
							</a>
						</div>
						<div class="col-md-9 event-details">
							<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a><?php echo synved_social_share_markup(); ?><hr>
							<p><strong><?php the_field('event_date'); ?></strong> at </p>
							<!-- Event Venue -->
							<?php $post_objects = get_field('event_venue'); if( $post_objects ): ?>
						    <?php foreach( $post_objects as $post_object): ?>
						    <a href="<?php the_field('venue_website', $post_object); ?>" target="_blank">
						    <?php echo get_the_title($post_object); ?>
							</a>
						    <?php endforeach; ?>
							<?php endif; ?>
							<br>
							<?php the_content() ?>

							<div class="facebook-button">
								<?php if (get_field('event_facebook_page')): ?>
								<a href="<?php the_field('event_facebook_page'); ?>" target="_blank"><button class="btn btn-primary">FACEBOOK EVENT PAGE</button></a>
								<?php endif; ?>
							</div>

							<!-- Event Dj's -->
							<?php $post_objects = get_field('event_djs'); if( $post_objects ): ?>
						    <div class="row text-center event-members">
						    <?php foreach( $post_objects as $post_object): ?>
						    	<div class="col-md-2 col-xs-6">
						    	<a href="<?php echo get_permalink($post_object); ?>">
							    	<img src="<?php the_field('member_icon', $post_object); ?>">
							        <?php echo get_the_title($post_object); ?>
						        </a>
						        </div>
						    <?php endforeach; ?>
						    </div>
							<?php endif; ?>

						</div>
					</div>
				</div>
				
				<?php endif; ?>

			<?php endwhile; wp_reset_postdata();?>
		
		<?php endif; ?>
	
</div>