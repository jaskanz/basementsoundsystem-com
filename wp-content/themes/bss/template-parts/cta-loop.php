<?php $ctabg = get_field('cta_background', 7); ?>
<?php $ctatxt = get_field('cta_text', 7); ?>

<div class="container-fluid parallax-window" id="cta" data-bleed="50" data-parallax="scroll" data-image-src="<?php echo $ctabg; ?>">


    <div class="row">
        <div class="container">
            <div class="col-sm-offset-3 col-sm-6 text-center cta-box">
                <h3><?php echo $ctatxt; ?></h3>
                <a class="btn" href="<?php bloginfo( 'url'); ?>/contact"><i class="fa fa-envelope"></i>CONTACT US</a>
            </div>
        </div>
    </div>
</div>
