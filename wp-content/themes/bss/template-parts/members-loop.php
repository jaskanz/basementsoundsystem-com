<!-- container-fluid -->
<div class="container-fluid member-container">
	
	<!-- container -->
	<div class="container">
		<?php if(!is_page_template( 'page-members.php' )) { $max = 4; $order = 'rand'; $viewall = TRUE;} else { $max = 999; } ?>			
		<?php $args = array('orderby' => $order, 'post_type' => 'member', 'posts_per_page' => $max); $posts = get_posts( $args)
		; foreach($posts as $post): setup_postdata( $post ); ?>
		
		<!-- column -->
		<a href="<?php the_permalink(); ?>" class="col-md-3 col-sm-6 member-col center">	
			<div class="member-pic-hover" style="background-image: url('<?php the_field('member_icon'); ?>');">
				<?php the_post_thumbnail('member-image', array( 'class' => 'member-image' )); ?>
			</div>	
			<div class="member-name">
				<h4><?php the_title(); ?></h4>
			</div>
		</a>

		<?php endforeach; wp_reset_postdata(); ?>
	</div>

	<div class="row center">
		<?php if ($viewall == TRUE) : ?>
			<a class="btn" href="<?php bloginfo( 'url'); ?>/members"><i class="fa fa-group"></i>VIEW ALL MEMBERS</a>
		<?php endif ;?>
	</div>
	
</div>