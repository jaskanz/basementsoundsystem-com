<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff"><title>Basement Sound System</title>

<meta name="description" itemprop="description" content="Basement Sound System is a DJ and Producer collective based out of St. Louis" />

<meta name="keywords" itemprop="keywords" content="dj, stl, saintlouis, saint, louis, wedding, music, deejay, crew, hiphop, edm, trap, dancing, atomic, cowboy, handlebar, " />
    <title>BASEMENT SOUND SYSTEMZ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
 
    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<!-- navigation -->
<div id="main-nav" class="navbar navbar-fixed-top shadow" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php bloginfo ('url'); ?>" class="navbar-brand smoothScroll">    <img src="<?php bloginfo ('url'); ?>/wp-content/uploads/2015/11/bss-nav-01.svg"/></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_id' => 'main-menu', 'menu_class' => 'nav navbar-nav navbar-right', 'container' => 'nav' ) ); ?>
        </div>
    </div>
</div>

<div id="main">