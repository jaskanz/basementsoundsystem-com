<?php
/**
 * Template Name: Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );?>
<?php $ctatxt = get_field('cta_text', 92); ?>


<div class="container-fluid interior-header">
	<div class="container">
	    <h2><?php the_title(); ?></h2>
    </div>
</div>


<div class="container-fluid contact-container">  
	<div class="container">
        <div class="col-md-9">
        <h3><?php echo $ctatxt; ?></h3>
        <?php 
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); 
				the_content();
			} 
		} ?>
		</div>
		<div id="contact-hero" class="col-md-3" style="background-image: url(<?php echo $src[0]; ?> )">
		</div>
        

	</div>
</div>

<?php get_footer(); ?>